import { RecipeService } from './../recipe.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'rb-recipe-item',
  templateUrl: './recipe-item.component.html'
})
export class RecipeItemComponent  {

  @Input() recipe: Recipe;
  @Input() recipeId: number;
  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private recipeService: RecipeService) {};
}
