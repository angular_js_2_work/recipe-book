import { Subscription } from 'rxjs/Subscription';
import { RecipeService } from './../recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Recipe } from 'app/recipes/recipe';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Ingredient } from "app/shared/ingredient";

@Component({
  selector: 'rb-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styles: []
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private recipe: Recipe;
  private recipeIndex: number;
  private isNew = true;

  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private recipeService: RecipeService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.recipeIndex = +params['id'];
          this.isNew = false;
          this.recipe = this.recipeService.getRecipe(this.recipeIndex);
        } else {
          this.isNew = true;
          this.recipe = null;
        }
       this.initForm();
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  private initForm() {
    let recipeName = '';
    let recipeImageUrl = '';
    let recipeContent = '';
    const recipeIngredients: FormArray = new FormArray([]);

    if (!this.isNew) {
      this.recipe.ingredients.forEach(ingredient => {
        recipeIngredients.push(
          new FormGroup({
            name: new FormControl(ingredient.name, Validators.required),
            amount: new FormControl(ingredient.amount, [Validators.required, Validators.pattern('\\d+')])
          })
        );
      });
      recipeName = this.recipe.name;
      recipeImageUrl = this.recipe.imagePath;
      recipeContent = this.recipe.description;
    }
    this.recipeForm = this.formBuilder.group({
      name: [recipeName, Validators.required],
      imagePath: [recipeImageUrl, Validators.required],
      description: [recipeContent, Validators.required],
      ingredients: recipeIngredients
    });
    console.log((<FormArray>this.recipeForm.controls['ingredients']).controls);
  }


  onSubmit() {
    const newRecipe = this.recipeForm.value;
    if ( this.isNew ) {
      this.recipeService.addRecipe(newRecipe);
    } else {
      this.recipeService.editRecipe(this.recipe, newRecipe);
    }
    this.navigateBack();
  }

  onCancel() {
    this.navigateBack();
  }
  private navigateBack() {
    this.router.navigate(['../']);
  }

  onAddItem(name: string, amount: string) {
      const newIngredient = new Ingredient(name, +amount);
      (<FormArray>this.recipeForm.controls['ingredients']).push(new FormGroup({
            name: new FormControl(newIngredient.name, Validators.required),
            amount: new FormControl(newIngredient.amount, [Validators.required, Validators.pattern('\\d+')])
          }));
     this.recipeService.addIngredient(this.recipe, newIngredient);
  }

  onRemoveItem(itemNumber: number) {
    (<FormArray>this.recipeForm.controls['ingredients']).removeAt(itemNumber);
    this.recipeService.removeIngredient(this.recipe, itemNumber);
  }
}
