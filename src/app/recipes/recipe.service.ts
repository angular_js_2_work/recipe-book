import { Injectable } from '@angular/core';
import { Recipe } from './recipe';
import { Ingredient } from '../shared/ingredient';

@Injectable()
export class RecipeService {

  private recipes: Recipe[] = [
    new Recipe('Schnitzel', 'Very Tasty', 'http://www.daringgourmet.com/wp-content/uploads/2014/03/Schnitzel-5.jpg',
      [new Ingredient('French Fries', 2), new Ingredient('Pork Meat', 1)]),
    new Recipe('Summer Salad', 'Okayish', 
      'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/beetroot-feta-grain-salad.jpg',
      [new Ingredient('salt', 10), new Ingredient('pepper', 5), new Ingredient('salad', 200)])
  ];

  constructor() { }

  getRecipes(): Recipe[] {
    return this.recipes;
  }

  getRecipe(id: number): Recipe {
    return this.recipes[id];
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
  }

  editRecipe(oldRecipe: Recipe, newRecipe: Recipe) {
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe;
  }

  deleteRecipe(recipe: Recipe) {
    /// using splice to delete the current recipe, therefor using splice with 1
    this.recipes.splice(this.recipes.indexOf(recipe), 1);
  }

  addIngredient(toRecipe: Recipe, ingredient: Ingredient) {
    this.recipes[this.recipes.indexOf(toRecipe)].ingredients.push(ingredient);
  }

  removeIngredient(toRecipe: Recipe, itemNumber: number) {
    const recipeIndex = this.recipes.indexOf(toRecipe);
    // const ingredientIndex = this.recipes[recipeIndex].ingredients.indexOf(ingredient);
    const ingredientIndex = itemNumber;
    this.recipes[recipeIndex].ingredients.splice(ingredientIndex, 1);
  }
}
